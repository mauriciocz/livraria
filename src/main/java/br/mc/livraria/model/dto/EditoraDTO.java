package br.mc.livraria.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditoraDTO {
	private Long id;
	@NotBlank(message="O campo nome deve estar preenchido") 
	@Size(message = "O campo nome deve ter entre 3 e 255 caracteres", max = 255, min = 3)
	private String nome;
	@NotBlank(message="O campo descricao deve estar preenchido") 
	@Size(message = "O campo descricao deve ter entre 3 e 255 caracteres", max = 100, min = 3)
	private String descricao;
}
