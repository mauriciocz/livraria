package br.mc.livraria.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LivroDTO {
	private Long id;
	@NotBlank(message="O campo nome deve estar preenchido") 
	@Size(message = "O campo nome deve ter entre 3 e 100 caracteres", max = 100, min = 3)
	private String nome;
	@NotBlank(message="O campo isbn deve estar preenchido") 
	@Size(message = "O campo isbn deve ter 13 caracteres", max = 13, min = 13)
	private String isbn;
	private CategoriaDTO categoria;
	private EditoraDTO editora;
}
