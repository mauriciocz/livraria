package br.mc.livraria.model.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoriaDTO {
	private Long id;
	@NotBlank(message="O campo nome deve estar preenchido") 
	@Size(message = "O campo nome deve ter entre 3 e 100 caracteres", max = 100, min = 3)
	private String nome;
}
