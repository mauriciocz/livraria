package br.mc.livraria.model.mapper;

import org.mapstruct.Mapper;

import br.mc.livraria.model.dto.LivroDTO;
import br.mc.livraria.model.entity.Livro;

@Mapper(componentModel = "spring")
public interface LivroMapper extends BaseMapper<Livro, LivroDTO> {

}
