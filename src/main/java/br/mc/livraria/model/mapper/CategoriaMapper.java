package br.mc.livraria.model.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.mc.livraria.model.dto.CategoriaDTO;
import br.mc.livraria.model.entity.Categoria;

@Mapper(componentModel = "spring")
public interface CategoriaMapper extends BaseMapper<Categoria, CategoriaDTO> {

	@Mapping(target = "categoria.livros", ignore = true)
	CategoriaDTO parseDTO(Categoria categoria);
	
}
