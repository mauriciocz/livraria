package br.mc.livraria.model.mapper;

import java.util.List;

public interface BaseMapper<T,U> {
	List<U> parseListDTO(List<T> entities);
	List<T> parseListEntity(List<U> dtos);
	U parseDTO(T entity);
	T parseEntity(U dto);
}
