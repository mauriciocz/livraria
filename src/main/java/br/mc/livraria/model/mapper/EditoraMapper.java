package br.mc.livraria.model.mapper;

import org.mapstruct.Mapper;

import br.mc.livraria.model.dto.EditoraDTO;
import br.mc.livraria.model.entity.Editora;

@Mapper(componentModel = "spring")
public interface EditoraMapper extends BaseMapper<Editora, EditoraDTO> {
}
