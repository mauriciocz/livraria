package br.mc.livraria.model.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "editora")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Editora {
	
	public Editora(String nome, String descricao) {
		this.nome = nome;
		this.descricao = descricao;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="nome", nullable=false, unique=true)
	private String nome;
	
	@Column(name="descricao", nullable=false)
	private String descricao;
	
	@OneToMany(mappedBy = "editora")
	private List<Livro> livros;
}
