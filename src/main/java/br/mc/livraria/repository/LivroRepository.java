package br.mc.livraria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.mc.livraria.model.entity.Categoria;
import br.mc.livraria.model.entity.Editora;
import br.mc.livraria.model.entity.Livro;

@Repository
public interface LivroRepository extends JpaRepository<Livro, Long> {
	List<Livro> findByNomeContainingIgnoreCase(String nome);
	List<Livro> findByEditora(Editora editora);
	List<Livro> findByCategoria(Categoria categoria);
}
