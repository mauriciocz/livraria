package br.mc.livraria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.mc.livraria.model.entity.Categoria;
import br.mc.livraria.model.entity.Editora;

@Repository
public interface EditoraRepository extends JpaRepository<Editora, Long>{

}
