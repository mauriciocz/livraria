package br.mc.livraria.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQueryFactory;

import br.mc.livraria.model.entity.Livro;
import br.mc.livraria.model.entity.QLivro;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Repository
public class LivroFilterRepository extends QuerydslRepositorySupport {

	public LivroFilterRepository() {
		super(Livro.class);
	}
	
	@PersistenceContext
	private EntityManager em;

	public List<Livro> filtrar(Livro filter){
		QLivro livro = QLivro.livro;
		
		List<Predicate> predicates = new ArrayList<>();

		if(StringUtils.hasText(filter.getNome())) {
			predicates.add(livro.nome.likeIgnoreCase("%" + filter.getNome() + "%"));
		}
		
		if(StringUtils.hasText(filter.getIsbn())) {
			predicates.add(livro.isbn.likeIgnoreCase("%" + filter.getIsbn() + "%"));
		}
		
		return new JPAQueryFactory(em).selectFrom(livro).where(
					predicates.toArray(new Predicate[0])
				).fetch();
	}
}
