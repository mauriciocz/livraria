package br.mc.livraria.service;

import java.util.List;

public interface BaseService<T> {
	
	List<T> getAll();
	T get(Long id);
	T create(T entidade);
	T update(Long id, T entidade);
	void delete(Long id);
}
