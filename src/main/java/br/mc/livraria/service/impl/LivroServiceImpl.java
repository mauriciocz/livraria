package br.mc.livraria.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mc.livraria.model.dto.LivroDTO;
import br.mc.livraria.model.entity.Categoria;
import br.mc.livraria.model.entity.Editora;
import br.mc.livraria.model.entity.Livro;
import br.mc.livraria.model.mapper.LivroMapper;
import br.mc.livraria.repository.LivroFilterRepository;
import br.mc.livraria.repository.LivroRepository;
import br.mc.livraria.service.LivroService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

@Service
public class LivroServiceImpl implements LivroService {

	@Autowired
	private LivroRepository repository;
	
	@Autowired
	private LivroFilterRepository filterRepository;
	
	@Autowired
	private LivroMapper mapper;
	
	@PersistenceContext
	private EntityManager em;
	
	public List<LivroDTO> getAll() {
		return mapper.parseListDTO(repository.findAll());
	}
	
	public List<LivroDTO> findByNome(String nome) {
		List<Livro> Livros = repository.findByNomeContainingIgnoreCase(nome);
	
		return mapper.parseListDTO(Livros);
	}
	
	public List<LivroDTO> findByEditora(long idEditora) {
		Editora editora = new Editora();
		editora.setId(idEditora);
		List<Livro> Livros = repository.findByEditora(editora);
	
		return mapper.parseListDTO(Livros);
	}
	
	public List<LivroDTO> findByCategoria(long idCategoria) {
		Categoria categoria = new Categoria();
		categoria.setId(idCategoria);
		List<Livro> Livros = repository.findByCategoria(categoria);
	
		return mapper.parseListDTO(Livros);
	}
	
	public List<LivroDTO> filter(LivroDTO livroDTO) {
		Livro livro = mapper.parseEntity(livroDTO);
		List<Livro> livros = filterRepository.filtrar(livro);
		return mapper.parseListDTO(livros);
	}
	
	public LivroDTO get(Long id) {
		Optional<Livro> LivroOp = repository.findById(id);
		if(LivroOp.isPresent()) {
			Livro Livro = LivroOp.get();
			return mapper.parseDTO(Livro);
		}
		
		throw new EntityNotFoundException();
	}
	
	@Transactional
	public LivroDTO create(LivroDTO LivroDTO) {
		Livro Livro = mapper.parseEntity(LivroDTO);
		Livro.setId(null);
		repository.save(Livro);
		em.refresh(Livro);
		return mapper.parseDTO(Livro);
	}
	
	@Transactional
	public List<LivroDTO> create(List<LivroDTO> LivrosDTO) {
		List<Livro> Livros = mapper.parseListEntity(LivrosDTO);
		repository.saveAll(Livros);
		return mapper.parseListDTO(Livros);
	}
	
	public LivroDTO update(Long id, LivroDTO LivroDTO) {
		
		if(repository.existsById(id)) {
			Livro Livro = mapper.parseEntity(LivroDTO);
			Livro.setId(id);
			Livro = repository.save(Livro);
			return mapper.parseDTO(Livro);
		}
		
		throw new EntityNotFoundException();
	}
	
	public void delete(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
}
