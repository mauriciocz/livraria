package br.mc.livraria.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mc.livraria.model.dto.CategoriaDTO;
import br.mc.livraria.model.entity.Categoria;
import br.mc.livraria.model.mapper.CategoriaMapper;
import br.mc.livraria.repository.CategoriaRepository;
import br.mc.livraria.service.CategoriaService;
import jakarta.persistence.EntityNotFoundException;

@Service
public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	private CategoriaRepository repository;
	
	@Autowired
	private CategoriaMapper mapper;
	
	public List<CategoriaDTO> getAll() {
		return mapper.parseListDTO(repository.findAll());
	}
	
	public CategoriaDTO get(Long id) {
		Optional<Categoria> CategoriaOp = repository.findById(id);
		if(CategoriaOp.isPresent()) {
			Categoria categoria = CategoriaOp.get();
			return mapper.parseDTO(categoria);
		}
		
		throw new EntityNotFoundException();
	}
	
	public CategoriaDTO create(CategoriaDTO categoriaDTO) {
		Categoria categoria = mapper.parseEntity(categoriaDTO);
		categoria.setId(null);
		repository.save(categoria);
		return mapper.parseDTO(categoria);
	}
	
	public CategoriaDTO update(Long id, CategoriaDTO categoriaDTO) {
		
		if(repository.existsById(id)) {
			Categoria categoria = mapper.parseEntity(categoriaDTO);
			categoria.setId(id);
			categoria = repository.save(categoria);
			return mapper.parseDTO(categoria);
		}
		
		throw new EntityNotFoundException();
	}
	
	public void delete(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}
}
