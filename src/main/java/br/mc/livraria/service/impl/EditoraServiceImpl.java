package br.mc.livraria.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.mc.livraria.model.dto.EditoraDTO;
import br.mc.livraria.model.entity.Editora;
import br.mc.livraria.model.mapper.EditoraMapper;
import br.mc.livraria.repository.EditoraRepository;
import br.mc.livraria.service.EditoraService;
import jakarta.persistence.EntityNotFoundException;

@Service
public class EditoraServiceImpl implements EditoraService {

	@Autowired
	private EditoraRepository repository;
	
	@Autowired
	private EditoraMapper mapper;
	
	@Override
	public List<EditoraDTO> getAll() {
		return mapper.parseListDTO(repository.findAll());
	}

	@Override
	public EditoraDTO get(Long id) {
		Optional<Editora> EditoraOp = repository.findById(id);
		if(EditoraOp.isPresent()) {
			Editora Editora = EditoraOp.get();
			return mapper.parseDTO(Editora);
		}
		
		throw new EntityNotFoundException();
	}

	@Override
	public EditoraDTO create(EditoraDTO entidade) {
		Editora Editora = mapper.parseEntity(entidade);
		Editora.setId(null);
		repository.save(Editora);
		return mapper.parseDTO(Editora);
	}

	@Override
	public EditoraDTO update(Long id, EditoraDTO entidade) {
		
		if(repository.existsById(id)) {
			Editora Editora = mapper.parseEntity(entidade);
			Editora.setId(id);
			Editora = repository.save(Editora);
			return mapper.parseDTO(Editora);
		}
		
		throw new EntityNotFoundException();
	}

	@Override
	public void delete(Long id) {
		if(!repository.existsById(id)) {
			throw new EntityNotFoundException();
		}
		
		repository.deleteById(id);
	}

}
