package br.mc.livraria.service;

import br.mc.livraria.model.dto.EditoraDTO;

public interface EditoraService extends BaseService<EditoraDTO>{

}
