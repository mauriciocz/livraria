package br.mc.livraria.service;

import java.util.List;

import br.mc.livraria.model.dto.LivroDTO;

public interface LivroService extends BaseService<LivroDTO>{

	
	List<LivroDTO> findByNome(String nome);
	
	List<LivroDTO> findByEditora(long idEditora);
	
	List<LivroDTO> findByCategoria(long idCategoria);
	
	List<LivroDTO> filter(LivroDTO livroDTO);
}
