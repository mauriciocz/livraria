package br.mc.livraria.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.mc.livraria.model.dto.LivroDTO;
import br.mc.livraria.service.LivroService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/livro")
public class LivroController extends BaseController<LivroDTO, LivroService> {

	public LivroController(LivroService service) {
		super(service);
	}


	@GetMapping("/byCategoria/{id}")
    public ResponseEntity<List<LivroDTO>> findByCategoria(@PathVariable("id") Long id) {
    	log.info(this.getClass().getSimpleName() + ".findByCategoria(" + id + ")");
    	List<LivroDTO> entities = service.findByCategoria(id);
    	log.info(this.getClass().getSimpleName() + ". entities: " + entities);
        return ResponseEntity.ok(entities);
    }


	@GetMapping("/byEditora/{id}")
    public ResponseEntity<List<LivroDTO>> findByEditora(@PathVariable("id") Long id) {
    	log.info(this.getClass().getSimpleName() + ".findByEditora(" + id + ")");
    	List<LivroDTO> entities = service.findByEditora(id);
    	log.info(this.getClass().getSimpleName() + ". entities: " + entities);
        return ResponseEntity.ok(entities);
    }
	

	
	@PostMapping("/filter")
    public ResponseEntity<List<LivroDTO>> filtrar(@RequestBody LivroDTO entidade) {
        try {
        	
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(service.filter(entidade));

        }catch(Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
