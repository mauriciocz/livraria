package br.mc.livraria.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.mc.livraria.model.dto.EditoraDTO;
import br.mc.livraria.model.dto.LivroDTO;
import br.mc.livraria.service.EditoraService;
import br.mc.livraria.service.LivroService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/editora")
public class EditoraController extends BaseController<EditoraDTO, EditoraService> {

	private LivroService livroService;
	
	public EditoraController(EditoraService service, LivroService livroService) {
		super(service);
		this.livroService = livroService;
	}


	@Override
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
    	log.info(this.getClass().getSimpleName() + ".delete(" + id + ")");
		List<LivroDTO> livros = livroService.findByEditora(id);
    	log.info(this.getClass().getSimpleName() + ". livros: " + livros);
		if (livros != null && !livros.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("A editora tem livros associados");
		}
		return super.delete(id);
	}
}
