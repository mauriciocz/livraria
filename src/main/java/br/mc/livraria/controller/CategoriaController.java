package br.mc.livraria.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.mc.livraria.model.dto.CategoriaDTO;
import br.mc.livraria.model.dto.LivroDTO;
import br.mc.livraria.service.CategoriaService;
import br.mc.livraria.service.LivroService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/categoria")
public class CategoriaController extends BaseController<CategoriaDTO, CategoriaService> {

	private LivroService livroService;
	
	public CategoriaController(CategoriaService service, LivroService livroService) {
		super(service);
		this.livroService = livroService;
	}

	@Override
	public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
    	log.info(this.getClass().getSimpleName() + ".delete(" + id + ")");
		List<LivroDTO> livros = livroService.findByCategoria(id);
    	log.info(this.getClass().getSimpleName() + ". livros: " + livros);
		if (livros != null && !livros.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("A categoria tem livros associados");
		}
		return super.delete(id);
	}

	
}
