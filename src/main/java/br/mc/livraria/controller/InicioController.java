package br.mc.livraria.controller;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.mc.livraria.model.entity.Categoria;
import br.mc.livraria.model.entity.Editora;
import br.mc.livraria.model.entity.Livro;
import br.mc.livraria.repository.CategoriaRepository;
import br.mc.livraria.repository.EditoraRepository;
import br.mc.livraria.repository.LivroRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/inicio")
public class InicioController {
	
	@Autowired
	private CategoriaRepository catRep;
	
	@Autowired
	private EditoraRepository editRep;
	
	@Autowired
	private LivroRepository livroRep;

    @GetMapping
    public ResponseEntity<String> iniciar() {
    	log.info(this.getClass().getSimpleName() + ".iniciar()");
    	

    	Editora ed1 = new Editora("Editora 1", "Descrição da Editora 1");
    	Editora ed2 = new Editora("Editora 2", "Descrição da Editora 2");
    	Editora ed3 = new Editora("Editora 3", "Descrição da Editora 3");
    	
    	List<Editora> eds = Arrays.asList(ed1, ed3);
    	
    	editRep.saveAll(Arrays.asList(ed1, ed2, ed3));
    	log.info(this.getClass().getSimpleName() + ". Editoras: " + editRep.findAll());

    	Categoria c1 = new Categoria("Biografia");
    	Categoria c2 = new Categoria("Terror");
    	Categoria c3 = new Categoria("Romance");
    	Categoria c4 = new Categoria("Ficção Científica");
    	Categoria c5 = new Categoria("Técnico");
    	catRep.saveAll(Arrays.asList(c1,c2,c3,c4,c5));
    	log.info(this.getClass().getSimpleName() + ". Categorias: " + catRep.findAll());
    	
    	List<Categoria> cats = Arrays.asList(c2,c3,c4);
    	
    	String[] ss = {"AAA", "BBB", "CCC"}; 
    	for (int i = 0; i < 10; i++) {
    		String isbn = UUID.randomUUID().toString().substring(0, 13);
    		Livro l = new Livro(null, "Livro " + (i + 1) + ss[i % 3], isbn, cats.get(i % 3), eds.get(i % 2));
    		livroRep.save(l);
    	}
    	
        return ResponseEntity.ok("Iniciado");
    }
}
